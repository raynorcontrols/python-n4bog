from typing import List
from xml.etree.ElementTree import Element

from n4bog.slots.niagara_slot import Slot


class Component(Slot):
    """
    The parent type of all components.
    """
    def __init__(self, name: str, n4module: str, n4type: str):
        """
        Creates a new component
        :param name: Name of the component
        :param n4module: The niagara module the component is in
        :param n4type: The niagara type the component is in
        """
        name = self.to_valid_name(name)
        super().__init__(name=name, needs_handle=True, niagara_module=n4module, niagara_type=n4type)

    @staticmethod
    def to_valid_name(name: str) -> str:
        """
        Hexadecimal-encodes characters that are invalid for a N4 component name.
        :param name: Possibly non-valid name
        :return: Fully valid N4 name
        """
        # noinspection SpellCheckingInspection
        valid_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789_"
        valid_name = ""
        for character in name:
            if character in valid_chars:
                valid_name += character
            else:
                # hex-encode the char if it isn't valid
                valid_name += "%"
                valid_name += format(ord(character), "02X")
        return valid_name

    # we suppress this inspection because child classes may change behavior for
    # deciding if a component is a valid child
    # noinspection PyMethodMayBeStatic
    def is_valid_child(self, child) -> bool:
        """
        Checks if a component can be a child of this component
        :type child: Component
        :return: If the component is a valid child of this component
        """
        return True

    def add_child(self, child: Slot):
        """
        Adds the component as a child of this component
        :param child: Child component to add
        :return:
        """
        if isinstance(child, Component):
            if not self.is_valid_child(child):
                from n4bog.exceptions import InvalidChildException
                raise InvalidChildException(child.name)
        super().add_child(child)