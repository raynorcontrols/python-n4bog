from .niagara_component import Component
from .niagara_slot import Slot


class UnrestrictedFolder(Slot):
    def __init__(self, name=None):
        super().__init__(name=name, niagara_module="baja", niagara_type="UnrestrictedFolder")


class Facets(Slot):
    def __init__(self, name="facets", value: str = None):
        super().__init__(name=name, niagara_module="baja", niagara_type="Facets", value=value)

    def set_value(self, value: str):
        self.value = value
