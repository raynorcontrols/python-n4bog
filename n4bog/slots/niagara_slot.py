from typing import Union, List
from xml.etree.ElementTree import Element

from n4bog.exceptions import InvalidTypeException
from n4bog.niagara_scope import NiagaraScope


class Slot:
    def __init__(self, name: Union[str, None] = None, needs_handle: bool = False,
                 niagara_module: Union[str, None] = None, niagara_type: Union[str, None] = None,
                 value: Union[str, None] = None):
        """
        A slot is anything that may exist in the Niagara object graph, and may have children, a name, a type, etc.
        :param name: The name (`n` XML field) of the slot
        :param needs_handle: If the object needs a handle in the object graph (`h` slot)
        :param niagara_module: Palette the type is from (first half of `t`, and also `m` if not previously defined)
        :param niagara_type: The type from the specified module/palette. (second half of `t`)
        :param value: Value of the slot (`v`)
        """
        self.name: Union[str, None] = name
        self.needs_handle = needs_handle
        self.module: Union[str, None] = None
        self.type: Union[str, None] = None
        if niagara_type is not None or niagara_module is not None:
            if niagara_module is not None and niagara_type is not None:
                # both are defined, so lets just set both now
                self.module = niagara_module
                self.type = niagara_type
            elif niagara_module is None and niagara_type is not None:
                # niagara_type specified and no niagara_module - likely mod:type shorthand
                # check if niagara_type has a `:` in it, and then define both from the shorthand syntax
                if ":" in niagara_type:
                    self.module = niagara_type.split(":")[0]
                    self.module = niagara_type.split(":")[1]
                else:
                    raise InvalidTypeException("niagara_type has no ':', and no niagara_module specified")
            else:
                raise InvalidTypeException("niagara_module specified with no niagara_type")
        self.value: Union[str, None] = value
        self.children: List[Slot] = []

    def add_child(self, child):
        self.children.append(child)

    def encode(self, scope: NiagaraScope) -> Element:
        el = Element("p")
        if self.name is not None:
            el.attrib["n"] = self.name
        if self.needs_handle:
            el.attrib["h"] = scope.handle

        if self.module is not None:
            if scope.should_map(self.module):
                el.attrib["m"] = f"{scope.get_module(self.module)}={self.module}"
            el.attrib["t"] = f"{scope.get_module(self.module)}:{self.type}"
        if self.value is not None:
            el.attrib["v"] = self.value

        for child in self.children:
            el.append(child.encode(scope))

        return el

    def __str__(self):
        stringified = ''
        if self.module is not None:
            stringified += f"[{self.module}:{self.type}] "
        if self.name is not None:
            stringified += f"{self.name}"
        if self.needs_handle:
            stringified += " (has handle)"
        if self.value is not None:
            stringified += ": " + self.value
        return stringified

    def print(self, indent=0):
        stringified = ' '*indent
        stringified += str(self)

        print(stringified)
        indent += 2
        for child in self.children:
            child.print(indent)
