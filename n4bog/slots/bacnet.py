from .baja import Facets
from .niagara_component import Component
from . import control as c
from .niagara_slot import Slot


class BacnetPointDeviceExt(Component):
    def __init__(self):
        super().__init__(name="points", n4module="bacnet", n4type="BacnetPointDeviceExt")


class BacnetObjectIdentifier(Slot):
    def __init__(self, identifier: str):
        super().__init__(name="objectId", niagara_module="bacnet", niagara_type="BacnetObjectIdentifier",
                         value=identifier)


class BacnetBooleanProxyExt(Slot):
    def __init__(self, instance: int):
        super().__init__(name="proxyExt", needs_handle=True,
                         niagara_module="bacnet", niagara_type="BacnetBooleanProxyExt")
        self.device_facets = Facets("deviceFacets", BooleanPoint.default_facets)
        self.add_child(self.device_facets)
        self.object_id = BacnetObjectIdentifier(f"binaryValue:{instance}")
        self.add_child(self.object_id)
        self.add_child(c.DataType("ENUMERATED"))


class BacnetNumericProxyExt(Slot):
    def __init__(self, instance: int):
        super().__init__(name="proxyExt", needs_handle=True,
                         niagara_module="bacnet", niagara_type="BacnetNumericProxyExt")
        self.device_facets = Facets("deviceFacets", NumericPoint.default_facets)
        self.add_child(self.device_facets)
        self.object_id = BacnetObjectIdentifier(f"analogValue:{instance}")
        self.add_child(self.object_id)
        self.add_child(c.DataType("REAL"))


class BooleanPoint(c.BooleanPoint):
    def __init__(self, name: str, instance: int):
        super().__init__(name)
        self.proxy = BacnetBooleanProxyExt(instance)
        self.add_child(self.proxy)


class NumericPoint(c.NumericPoint):
    def __init__(self, name: str, instance: int):
        super().__init__(name)
        self.proxy = BacnetNumericProxyExt(instance)
        self.add_child(self.proxy)
