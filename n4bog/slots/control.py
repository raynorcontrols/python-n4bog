from .baja import Facets
from .niagara_component import Component
from .niagara_slot import Slot


class ControlPoint(Component):
    def __init__(self, name, n4module, n4type):
        super().__init__(name=name, n4module=n4module, n4type=n4type)


class BooleanPoint(ControlPoint):
    default_facets = "trueText=s:true|falseText=s:false"

    def __init__(self, name):
        super().__init__(name=name, n4module="control", n4type="BooleanPoint")
        self.facets = Facets(value=self.default_facets)
        self.add_child(self.facets)

    def set_facets(self, value: str):
        self.facets.set_value(value)


class NumericPoint(ControlPoint):
    default_facets = "units=u:null;;;;"

    def __init__(self, name):
        super().__init__(name=name, n4module="control", n4type="NumericPoint")
        self.facets = Facets(value=self.default_facets)
        self.add_child(self.facets)

    def set_facets(self, value: str):
        self.facets.set_value(value)


# unsure where this should go, to be honest? may be moved later.
class DataType(Slot):
    def __init__(self, data_type):
        super().__init__(name="dataType", value=data_type)
