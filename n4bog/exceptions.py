class InvalidChildException(Exception):
    def __init__(self, child_name: str):
        super().__init__(f"{child_name} is not a valid child here.")


class InvalidTypeException(Exception):
    pass
