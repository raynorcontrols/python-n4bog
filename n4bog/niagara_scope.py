class NiagaraScope:
    """
    Handles keeping track of handles and types used in a bog file
    """
    def __init__(self):
        self.last_handle = 0
        self.used_modules = {}

    @property
    def handle(self) -> str:
        """
        :return: The next available Niagara object handle
        """
        self.last_handle = self.last_handle + 1
        return f"{(self.last_handle-1):x}"

    def should_map(self, fullname: str) -> bool:
        """
        Checks if you need to add an `m` attribute to link a module's full name to a short name.
        :param fullname: Module's full name
        :return: If you need to add it to the `m` attribute
        """
        return fullname not in self.used_modules

    def get_module(self, fullname: str) -> str:
        """
        Gets the shortname for a module. Tracks if the module name was previously given, and marks it as used if not.
        Used modules will make `should_map` return false.
        :param fullname: Module's full name
        :return: Module's short name for this bog file
        """
        # check if the full name already has a mapping to a short name
        if fullname in self.used_modules:
            return self.used_modules[fullname]
        # since we don't have it, we have to figure out what name to use
        # niagara seems to have some kind of per-module mapping for short names, and i really only know a few of them
        # right now, but I think we can just assume that a module's short name is the first letter, then throw an
        # exception if it's taken. for the cases where that's wrong, we have a `special_names` dict.
        # I'm honestly unsure if this is needed, though? does it work with any short name we give it? need to test it...
        special_names = {
            "bacnet": "bac"
        }
        if fullname in special_names:
            self.used_modules[fullname] = special_names[fullname]
            return special_names[fullname]

        shortname = fullname[0]
        # reverses self.used_modules to be keyed based on the short name vs full name
        # we turn off the PyTypeChecker exception here because this is, in fact, a valid way to create a dict
        # (ie. the lambda statement there generates the list contents for the dict)
        # noinspection PyTypeChecker
        module_shortnames = dict([reversed(kv) for kv in self.used_modules.items()])
        if shortname in module_shortnames:
            raise Exception(f"module shortname {shortname} already used when trying to allocate one for {fullname}")
        self.used_modules[fullname] = shortname
        return shortname