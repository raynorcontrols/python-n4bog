from ..bog_generator import BogGenerator
from ..slots.baja import UnrestrictedFolder
from ..slots.bacnet import BacnetPointDeviceExt, BooleanPoint, NumericPoint

if __name__ == "__main__":
    # the actual command line interface
    import argparse
    parser = argparse.ArgumentParser(description="Convert bactalk CSV/TXT exports into N4.x bog files with points",
                                     epilog="Note for exporting: I tested this by taking the `.mdb` file for the "
                                            "device, then right clicking the tables for each type and exporting as "
                                            "a text file with no formatting delimited with commas.")
    parser.add_argument("-s,--sanitize-names", help="Sanitize names to prevent escaping of invalid characters",
                        action="store_true", dest="sanitize")
    parser.add_argument("-b,--bv", help="CSV with list of binary value points. Can be specified multiple times.",
                        action="append", dest="bv_files", required=False)
    parser.add_argument("-a,--av", help="CSV with list of analog value points. Can be specified multiple times.",
                        action="append", dest="av_files", required=False)
    parser.add_argument("-m,--mv,--msv", help="CSV with list of multi-state value points. Can be specified multiple "
                                              "times. Note `--mv-enums` will need to be given to properly generate "
                                              "the values for the MSVs.",
                        action="append", dest="mv_files", required=False)
    parser.add_argument("-e,--mv-enums,--msv-enums", help="CSV with list of values for multi-state value points. Can "
                                                          "be specified multiple times. Won't do anything without at "
                                                          "least one matching multi-state value defined.",
                        action="append", dest="mv_enums", required=False)
    parser.add_argument("-o,--output,--output-file", help="Path to output bog file.", dest="output", required=True)
    args = parser.parse_args()

    """
    create the basic structure of the bog, which looks like this:
    - bajaObjectGraph (BogGenerator)
        - UnrestrictedFolder
            - points (BacnetPointDeviceExt)
    """
    bog = BogGenerator()
    root_folder = UnrestrictedFolder()
    points_folder = BacnetPointDeviceExt()
    root_folder.add_child(points_folder)
    bog.add_child(root_folder)

    def sanitize_name(name):
        if args.sanitize:
            valid_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789_"
            valid_name = ""
            for character in name:
                if character in valid_chars:
                    valid_name += character
                else:
                    # replace invalid chars with underscores
                    valid_name += "_"
            return valid_name
        return name

    # read the data in the files and add data to the component tree
    for file in args.bv_files:
        with open(file, encoding="utf-8") as f:
            import csv
            csvreader = csv.reader(f)
            for row in csvreader:
                points_folder.add_child(BooleanPoint(sanitize_name(row[1]), int(row[0])))

    for file in args.av_files:
        with open(file, encoding="utf-8") as f:
            import csv
            csvreader = csv.reader(f)
            for row in csvreader:
                points_folder.add_child(NumericPoint(sanitize_name(row[1]), int(row[0])))

    bog.print()
    bog.write_bog(args.output)
