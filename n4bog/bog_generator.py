from typing import List
import xml.etree.ElementTree as ET

from n4bog.niagara_scope import NiagaraScope
from n4bog.slots.niagara_slot import Slot


class BogGenerator(Slot):
    def __init__(self):
        super().__init__()
        self.children: List[Slot] = []

    def add_child(self, child):
        self.children.append(child)

    def encode(self, scope=None) -> str:
        """
        Generate the XML string representation of the bog file
        :return: bog XML
        """
        if scope is None:
            scope = NiagaraScope()
        bog = ET.Element("bajaObjectGraph")
        bog.attrib["version"] = "4.0"
        bog.attrib["reversibleEncodingKeySource"] = "none"
        bog.attrib["FIPSEnabled"] = "false"
        bog.attrib["reversibleEncodingValidator"] = "[null.1]"
        for child in self.children:
            bog.append(child.encode(scope))
        return self.prettify_xml(bog)

    def __str__(self):
        return f"bog file root"

    def write_bog(self, path):
        """
        Generate the bog file for the given component tree
        :param path:
        :return:
        """
        from zipfile import ZipFile
        with ZipFile(path, "w") as bogZip:
            bogZip.writestr("file.xml", self.encode())

    @staticmethod
    def prettify_xml(elem):
        from xml.dom import minidom
        rough_string = ET.tostring(elem, "utf-8")
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml("  ")
